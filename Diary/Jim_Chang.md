This diary file is written by Jim Chang in the course Professional skills for engineering the third industrial revolution.

# 2021-9-30 #

* The first lecture was fresh to me honestly.
* I am really inspired and puzzled by the presentation and the diary.
* I am surprised by the Christiane's interview with president Macron.
* I learn a lot about how to improve the stastics and also how to present it more clearly.
* We should always doubt about the statics but not entirely deny the data.
* What Christiane said makes me reflect on myself whether I got trap in the stratosphere created by the social media.
* Newsvoice topic:Humanitarian advocate Mari Malek: Think twice before you donate to big NGO’s
* Billions of dollars every year are given to huge NGOs working in Africa yet famine, wars and poverty continue that create suffering and inequality.
* More people die from starvation rather than Covid-19, At the same time, Africa is extremely efficient in food production, which is a paradox.
* We should not donate our money to the huge NGOs even though they may seem more credible.
* We can try to believe and donate our money to some little humanitarian organizations since what they do may be more transparent and pratical.