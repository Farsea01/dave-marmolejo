This diary file is written by Gabrielle Vania Tandjung H24075332 in the course Professional skills for engineering the third industrial revolution.  

# 2021-09-23 

* The Professor still discussed about conflicts on the second lecture, we were given more examples 
* There were some presentations on the second period, the Professor gave some inputs for improvements 
* The Professor also taught us how to use Github and how to make the diary properly 
* On the third period, the class discussed about the 4th Industrial Revolution that I didn't know about before 

# 2021-09-30 

* This week we talked about how to differentiate fake and real news 
* We also watched a video talking about how to spot bad statistics 
* As a Statistics student myself, that video was very interesting to watch 
* The professor talked again about diary and grouping problems since there are still some students who do not understand, me included 
* My group have not gotten the chance to present yet 
