Diary by Danny Hsu

CLASS 3 2018-09-27


1. There are many fake news in our society,and the way we could distinguish them is to question them.

2. The average number can't stand for all number of the group


CLASS 4 2018-10-04


1. There are three main forces that drive the economy,first is productivity growth,second is short term debt cycle
   ,and the last one is long term debt.
   
2. Spending drives the economy.

3. Productivity matters in the long run,and credit matters in the short run.

4. Every time you borrow,you make a cycle.

5. There are three methods to solve"debt burdens are too high"

   first : people businesses and governments cut their spending.
   
   second : debts are reduced through defaults and restructurings.
   
   third : wealth is redistributed from haves to the have-nots.
   
   forth : the central bank print new money.
   
CLASS 5 2018-10-11

1. Humans will become digital,and it happened to the speaker

2. Some people,like Jonh Lennon,imagine that without nationalism,the world will become a peaceful paradise.

3. The most prosperous and peaceful countries in the world,countries like Sweden,Switzerland and Japan
   ,have a very strong sense of nationalism.

4. Countries that lack a strong sense of nationalism,like Congo,Somalia and Afghanistan,tend to be violent and poor.

5. Fascism tells its people that their nation is supreme,and they don't need to care about anybody or anything 
   other than their nation.

6. In the 20th century, democracy and capitalism defeated fascism and communism because democracy was better 
   at processing data and making decisions.

7. Every presentation we need to select a claim and explan it with data.

8. The correct form of date is year-month-day .

CLASS 6 2018-10-18

1. Simply move your body has immediate effrects on increace our mood.

2. The prefrontal cortex, right behind your forehead, critical for things 
   like decision-making, focus, attention and your personality.
   
CLASS 8 2018-11-01

1. It is not easy to cheat our brain 

2. If there are no differences between a real choice and a manipulated choice, perhaps we make things up all the time.

3. Sweden is a coalition government consisting of the left and right wing.

4. It's hard to be mother.

5. Persing happy alway make people not happy.

6. The meaning of life is to develop an inner self.

7. There are four pillars of a meaningful life,and we can each create lives of meaning 

   by building some or all of these pillars in our lives.
   
   the first pillar is belong 
   
   the second is "what you give not what you want"
   
   the third pillar of meaning is about stepping beyond yourself
   
   and the last one is story telling.
   
CLASS 10 2018-11-15
   
1. Dopamine is the exact chemical that make us feel good when we smoke when we drink and when we gamble.

2. Socal media also bring about addiction ,but we have no age restrictions on it.

3. The technology we loved, could also be our undoing.

4. There's thing called Moore's law that makes the computers more and more efficient and cheaper.

CLASS 12 2018-11-29
1. We live on a human-dominated planet, putting unprecedented pressure on the systems on Earth.

2. In the past 50 years, such a sharp decline of ecosystem functions and services on the planet, 

   one of them being the ability to regulate climate on the long term, in our forests, land and biodiversity.

CLASS 15 2018-12-20

2018-12-20 (productive)
1. studying material mechanics
2. having a material mechanics exam at 7 p.m.
3. my roommate's birthday!

2018-12-21 (unproductive)
1. have baseball training.
2. singing in my room 
3. spending many hours on YouTube.

2018-12-22 (productive)
1. it's winnter solstice today, and I go to eat hot pot with my friends.
2. discussing reports with classmates.

2018-12-23 (unproductive)
1. I woke up in the afternoon.
2. playing guitar for hours.

2018-12-24 (productive)
1. having a trip in Chiayi.

2018-12-25 (productive)
1. Merry Christmas!
2. having dinner with baseball team in a great restaurant.
3. exchanging gift with baseball team.

2018-12-26 (unproductive)
1. my father's birthday!
2. I went to bed too late the before today, so I dosed off in class.

CLASS 17 2019-01-03

20190103
1. We discussed the report in the class and also decided on the theme of the video.
2. After returning from Taipei, I caught a cold and I was very uncomfortable with coughing
   ,so I felt unproductive today.
   
20190104
1. I took a film with my classmates in the afternoon, and the shooting process went smoothly
   ,so it's productive today.
   
20190105
1. My teammates and I discussed the video content at the mechanical department at about 2:30 in the afternoon,and
   I felt productive.

20190106
1. Next week is the final exam week. I stayed in the department for a whole day
   , but the cold didn’t seem to be better. (unproductive)
   
20190107
1. I started to think that my weight has become heavier
   , so I decided to eat less and take more stairs without taking the elevator. (productive)

20190108
1. I went to the park to run after reading the book in the evening. I felt productive after the exercise.

20190109
1. Finished the editing of the film, it’s a bit tired to take the time to edit the film during the test week
   , but it’s very fulfilling,and I felt productive today.
